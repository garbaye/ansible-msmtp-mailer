#!/usr/bin/env bash

FROMFEDORA='36'
PACKAGE='msmtp'

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
cd ${ABSDIR}

type -P docker >/dev/null && DOCKER=$(which docker)
type -P podman >/dev/null && DOCKER=$(which podman)

if ! JSONLINE=$(curl -s https://src.fedoraproject.org/_dg/bodhi_updates/rpms/${PACKAGE} | grep -e "fc${FROMFEDORA}\","); then
    echo "ERROR : No ${PACKAGE} release available for Fedora ${FROMFEDORA}"
    echo "Please check https://src.fedoraproject.org/rpms/${PACKAGE} to choose the right release."
    exit 1
fi

VERSION=$(echo "${JSONLINE}" | sed -e "s/^\s*\"stable\": \"${PACKAGE}-\(.*\)\.fc${FROMFEDORA}\",\s*$/\1/")

ARCH=$(${DOCKER} run --rm docker.io/library/rockylinux:8 uname -i)

${DOCKER} build -t localhost/docker-${PACKAGE}-build -f- . <<EOF
FROM docker.io/library/rockylinux:8
RUN dnf install -y epel-release dnf-plugins-core && \
dnf config-manager --set-enabled powertools && \
dnf install -y rpm-build gcc make gettext gnutls-devel libgsasl-devel libidn-devel libsecret-devel && \
useradd -m dontbuildasroot -s /bin/bash
USER dontbuildasroot
RUN rpmbuild --rebuild https://download-ib01.fedoraproject.org/pub/fedora/linux/updates/${FROMFEDORA}/Everything/source/tree/Packages/m/${PACKAGE}-${VERSION}.fc${FROMFEDORA}.src.rpm
EOF

[[ $? = 0 ]] && mkdir /tmp/dockerrpmbuild-$$

${DOCKER} build --no-cache -v /tmp/dockerrpmbuild-$$:/out:Z -t docker-${PACKAGE}-xfer -f- . <<EOF
FROM localhost/docker-${PACKAGE}-build
USER root
RUN mv /home/dontbuildasroot/rpmbuild/RPMS/*/*.rpm /out/
EOF

[[ $? == 0 ]] && mv -i /tmp/dockerrpmbuild-$$/*.rpm ${ABSDIR}/files/ &&
rm -rf /tmp/dockerrpmbuild-$$ &&

${DOCKER} rmi -f docker-${PACKAGE}-build &&
${DOCKER} rmi -f docker-${PACKAGE}-xfer
